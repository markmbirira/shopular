(function() {
  'use strict';

  angular.module('inventory').factory('InventoryService', InventoryService);

  /** 
   * InventoryService: Service for creating and manipulating items in localStorage
   * @params {void}
   * @return {Object} Of methods that make up this service.
   */
  function InventoryService() {
    let items = JSON.parse(localStorage.getItem('items')) || [];
    /**
      * How the items are stored in the localStorage
      [ 
        {"id":1507203428321,"name":"Book","price":24,"quantity":8,"color":"Yellow","discount":2},
        {"id":1507203838617,"name":"Ipad","price":30,"quantity":4,"color":"Grey","discount":2},
        {"id":1507204194551,"name":"Book","price":40,"quantity":4,"color":"Color","discount":2}
      ]
    */

    /**
     * Adds a new item to the inventory
     * @param {Object} item   item to add, has an id, name, price, quantity, color, and discount
     * @return {void}
     */
    function addItemToInventory(item) {
      if(typeof(item) !== 'object') {
        return false;
      }
      if(typeof(item.name) !== 'string' || item.name.length < 1) {
        return false;
      }
      /* setting price to be a number since all data coming through a form is a string */
      item.price = Number(item.price);
      if(isNaN(item.price)) {
        return false;
      }
      let id = Date.now(); // generate new ID
      
      let new_inventory_item =  {
        id: id,
        name: item.name,
        price: item.price,
        quantity: item.quantity,
        color: item.color,
        discount: item.discount
      };

      items.push(new_inventory_item);

      localStorage.setItem('items', angular.toJson(items));
    }

    function getAllItems() {
      return items;
    }

    return {
      addItemToInventory: addItemToInventory,
      getAllItems: getAllItems
    };
  }
}());
