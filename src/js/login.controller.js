(function() {
  'use strict';

  angular.module('inventory')
    .controller('LoginController', LoginController);

  LoginController.$inject = ['$scope','LoginService'];


  /**
   * Creates a log in controller so application can have miltiple users
   * @param {Function} LoginService this manages the users information
   */
  function LoginController($scope, LoginService) {
    let vm = this;
    vm.newUser = {};
    vm.users = LoginService.getUsers();
    vm.loggedInStatus = false;
    vm.loggedIn = {
      time: Date.now()
    }

    /* user object bound to the username input element */
    $scope.user = {
      username: '' /* populated by the form */
    }

    /**
     * Creates new user after logging in
     * @param {Object} user Information about user
     */
    vm.addUser = function addUser(user) {
      LoginService.addUser(user);
      vm.newUser = {};
    };

    /**
      * get user from the input element (via the $scope Object)
      * It is a parameter to vm.login(), giving the entered username
      * @return {String} username the user's  keyed-in username
      */
    vm.user = function user() {
      return $scope.user.username;
    };

    /**
      * logs in the user.
      * @param {String} username the user's username

      */
    vm.login = function login(username) {
      console.log('logging in', username);
      LoginService.loginUser(username);
      vm.loggedInStatus = true;
      vm.loggedIn.time = Date.now();
    };

    vm.removeUser = function removeUser(user) {
      LoginService.removeUser(user);
    };

  }

}());
