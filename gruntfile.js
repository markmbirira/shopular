'use strict';

module.exports = function(grunt) {
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    jshint: {
      files: ['gruntfile.js', 'src/**/*.js', 'test/**/*.js'],
      options: {}
    },
    watch: {
      files: ['<%= jshint.files %>'],
      tasks: ['jshint', 'karma']
    },
    karma: { // task name, already defined
      all: { // target name, you compose this yourself
          options: {
            frameworks: ['mocha', 'chai'],
            browsers: ['Chrome'],
            files: [
              'node_modules/angular/angular.js',
              'node_modules/angular-mocks/angular-mocks.js',
              'src/js/inventory.module.js',
              'src/js/**/*.js',
              'test/**/*.spec.js'
            ],
            singleRun: false,
          }
      },
    }
  });

  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-karma');

  grunt.registerTask('test', ['karma:dev:run']);
  grunt.registerTask('jshint', ['jshint']);
  grunt.registerTask('watch', ['watch']);
  grunt.registerTask('default', ['karma:dev:run']); // simple linting by default
};
