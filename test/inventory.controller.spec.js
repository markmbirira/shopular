(function() {
  'use strict';

  let expect = chai.expect;

  describe('InventoryController', function() {
    let InventoryController;
    let scope;
    let mockInventoryService = {};

    beforeEach(module('inventory'));

    beforeEach(module(function($provide) {
      $provide.value('InventoryService', mockInventoryService);
    }));

    beforeEach(inject(function($controller) {
      mockInventoryService.getAllItems = function getAllItems() {
        return []; // empty users by default
      };
      mockInventoryService.addItemToInventory = function addItemToInventory() {
        return true; // a successful user creation returns true
      };
      InventoryController = $controller('InventoryController',{
        $scope: scope,
        InventoryService: mockInventoryService
      });
    }));

    it('Inventory list should be an array', function() {
      expect(InventoryController.inventoryList).to.be.an('array');
    });

    it('Inital list should has length of 0', function() {
      expect(InventoryController.inventoryList.length).to.be.equal(0);
    })

    it('default sortType is name', function() {
      expect(InventoryController.sortType).to.be.equal('name');
    })

    it('NewItem should be an object', function(){
      expect(InventoryController.newItem).to.be.an('object');
    });

    it('sortReverse should return false as a value', function() {
      expect(InventoryController.sortReverse).to.be.equal(false);
    });

    it('changeSort method should reverse sort', function() {
      InventoryController.changeSort('name');
      expect(InventoryController.sortReverse).to.be.equal(true);
    });

 });

}());
