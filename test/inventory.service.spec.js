(function() {
  'use strict';

  let expect = chai.expect;

  describe('inventory service', function(){
    let InventoryService;
    let sampleItem = {"id":1507203428321,"name":"Book","price":24,"quantity":8,"color":"Yellow","discount":2};

    beforeEach(module('inventory'));

    beforeEach(inject(function(_InventoryService_){
      InventoryService = _InventoryService_;
    }));

    afterEach(function() {
      localStorage.removeItem('items');
    });

    it('getAllItems() provide us with an array of objects', function() {
      let shopularItems = InventoryService.getAllItems();
      expect(shopularItems).to.be.an('array');
    });

    it('should handle empty object', function() {
      InventoryService.addItemToInventory({});
      let shopularItems = InventoryService.getAllItems();
      expect(shopularItems.length).to.equal(0);
    });

    it('addItemToInventory() should add an correct item', function() {
      InventoryService.addItemToInventory(sampleItem);
      let shopularItems = InventoryService.getAllItems();
      expect(shopularItems.length).to.equal(1);
    });

    it('addItemToInventory() should reject an incorrect item', function() {
      expect(InventoryService.addItemToInventory({id: '2233', name: undefined})).to.equal(false);
    });

    it('addItemToInventory() should reject an empty item', function() {
      expect(InventoryService.addItemToInventory({})).to.equal(false);
    });

  });

}());
